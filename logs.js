'use strict';

//import libraries
const aws = require('aws-sdk');
const db = new aws.DynamoDB();
const s3 = new aws.S3();
const md5 = require('md5');
const _ = require('lodash');
const format = require('date-fns/format');

//set a base model object
var model = {
		_id: {"S" : ""},
		user_id: {"S" : ""},
		log_level: {"N": ""},
		device_info: {"S": ""},
		log_entry: {"S": ""},
		created_at: {"S": ""}
};

const recurseDynamoDBItems = (callback, params, itemArray) => {

	console.log("DynamoDB params: "+JSON.stringify(params));

	db.scan(params).promise()
		.then(data => {

			itemArray = itemArray.concat(data.Items);

			if (data.LastEvaluatedKey){

				setTimeout(function() {
					params['ExclusiveStartKey'] = data.LastEvaluatedKey;
					recurseDynamoDBItems(callback, params, itemArray);
				}, 1);

			}else {

				//store to S3 and redirect client to this download loc
				const now = Date.now();
				s3.putObject({
					Bucket: process.env.LOGS_S3_BUCKET,
					Key: 'logs_'+now+'.json',
					Body: JSON.stringify({ "Items": itemArray }),
					ACL: 'authenticated-read'
				}, function(err, data) {
					if (err) console.log(err, err.stack); // an error occurred

					const url = s3.getSignedUrl('getObject', {
						Bucket: process.env.LOGS_S3_BUCKET,
						Key: 'logs_'+now+'.json',
						Expires: 60
					});

					//send a success callback HTTP 200 response with the data
					return callback(null, {
						"statusCode": 200,
						"headers": {
							'Access-Control-Allow-Origin': '*',
							'Access-Control-Allow-Credentials': true,
						},
						"body": JSON.stringify({ "Url": url })
					});
				});
			}
		})
		.catch(err => {
			return callback(err);
		});
};

//------------ CREATE -------------
module.exports.create = (event, context, callback) => {

	//for debugging in AWS CloudWatch
	console.log("request: " + JSON.stringify(event));

	//parse the JSON string into a JS object
	var bodyJson = JSON.parse(event.body);

	try {

		//get all possible variables from JSON body
		var user_id = bodyJson.user_id;
		var log_level = bodyJson.log_level;
		var device_info = bodyJson.device_info;
		var log_entry = bodyJson.log_entry;
		var created_at = bodyJson.created_at;

		//if any attribute is missing, throw an error back in HTTP response
		if (!user_id || !log_level || !device_info || !log_entry) {
			console.log("ERROR: One of the required query parameters was not sent\n(user_id, log_level, device_info, log_entry)!");
			return callback(null, {
					"statusCode": 400,
					"headers": {
			      'Access-Control-Allow-Origin': '*',
			      'Access-Control-Allow-Credentials': true,
			    },
					"body": "One of the required query parameters was not sent\n(user_id, log_level, device_info, log_entry)!"
			});
		}

		//save the passed params to the model object
		var ms_timestamp = Date.now();
		model._id.S = md5(user_id+ms_timestamp);
		model.user_id.S = user_id;
		model.log_level.N = log_level;
		model.device_info.S = device_info;
		model.log_entry.S = log_entry;
		if (created_at) {
			model.created_at.S = created_at;
		}
		else {
			model.created_at.S = format(new Date(), 'YYYY-MM-DDTHH:mm:ss.SSSZ');
		}

		//set up params object used by AWS API call
		const params = {
			TableName: process.env.TABLE_NAME, //comes from serverless.yml
			Item: model
		};

		//AWS API call: to dynamodb to add a log entry item using the passed JSON data
		db.putItem(params, function (err, data) {
			if (err) {
				console.log("ERROR: "+JSON.stringify(err));
				return callback(null, {
						"statusCode": 500,
						"headers": {
				      'Access-Control-Allow-Origin': '*',
				      'Access-Control-Allow-Credentials': true,
				    },
						"body": JSON.stringify(err)
					});
			}

			//send a success callback HTTP 200 response
			console.log("SUCCESS: Remote log saved!");
			return callback(null, {
				  "statusCode": 200,
					"headers": {
			      'Access-Control-Allow-Origin': '*',
			      'Access-Control-Allow-Credentials': true,
			    },
				  "body": "Remote log saved!"
			});
		});
	}catch(error) {
		callback(error);
	}
};

//--------------- GET ---------------
module.exports.get = (event, context, callback) => {

	//for debugging in AWS CloudWatch
  console.log("request: " + JSON.stringify(event));

	try {

		//Must be called with at least 1 GET param
		if (!event.queryStringParameters) {
			return callback("No filter parameters were supplied (UserID, LogLevel, LogId)!");
		}

		var user_id = event.queryStringParameters.UserID;
		var log_level = event.queryStringParameters.LogLevel;
		var log_id = event.queryStringParameters.LogId;

		//create filter expressions for 1 or both possible params
		var expAttrVals = {}, expAttrNames = {}, filterExpParts = [], filterExp = "";
		if (user_id){
			expAttrNames["#user_id"] = "user_id";
			expAttrVals[":user_id"] = {
				S: user_id
			};
			filterExpParts.push("contains (#user_id, :user_id)");
		}
		if (log_level){
			expAttrNames["#log_level"] = "log_level";
			expAttrVals[":log_level"] = {
				N: log_level
			};
			filterExpParts.push("#log_level >= :log_level");
		}
		if (log_id) {
			expAttrNames["#_id"] = "_id";
			expAttrVals[":_id"] = {
				S: log_id
			};
			filterExpParts.push("contains (#_id, :_id)");
		}

		//Check one more time: must be called with at least 1 GET param
		if (_.isEmpty(expAttrNames)){
			return callback("No filter parameters were supplied (UserID, LogLevel, LogId)!");
		}

		//create a filter expression string from the expressions above
		var count = 0;
		filterExpParts.forEach(function(obj, index){
			if (count > 0){
				filterExp += " AND ";
			}
			filterExp += obj;
			count++;
		});

		//set up params object that uses the filter expression string for the AWS API call
		var dbParams = {
			ExpressionAttributeNames: expAttrNames,
			ExpressionAttributeValues: expAttrVals,
		  FilterExpression: filterExp,
			ConsistentRead: true,
		  TableName: process.env.TABLE_NAME //comes from serverless.yml
		};

		recurseDynamoDBItems(callback, dbParams, []);

	}catch(error) {
		callback(error);
	}
};
