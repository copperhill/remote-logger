# Remote Logger (Serverless)

This small serverless function saves and retrieves log metadata and a log entry per service call to and from an AWS DynamoDB table.

## Table of Contents

- [Installation](#installation)
  - [Node Modules](#node-modules)
  - [Serverless](#serverless)
  - [Deploy](#deploy)
- [Usage](#usage)
  - [Create](#create)
  - [Get](#get)
- [Support](#support)
- [License](https://bitbucket.org/copperhill/remote-logger/src/master/LICENSE)

## Installation

On your local Mac or PC, install node.js 6.10+ and npm 3.10.10+. Clone this repo to a local directory.

```sh
$ git clone https://bitbucket.org/copperhill/remote-logger.git
```

### Node Modules

Install node_modules in the project directory:

```sh
$ cd ~/remote-logger
$ npm install
```

### Serverless

1. Install [Serverless](https://serverless.com/) on your machine.

```sh
$ npm install serverless -g
```

2. [Sign up for an AWS Free Tier account](https://aws.amazon.com/s/dm/optimization/server-side-test/free-tier/free_np/) or using an existing account and [sign into the console](https://aws.amazon.com/console/)

3. [Create an Access Key on AWS using IAM](https://serverless.com/framework/docs/providers/aws/guide/credentials#creating-aws-access-keys)

4. On your machine, install the Access Key credentials for Serverless [like so](https://serverless.com/framework/docs/providers/aws/guide/credentials#using-aws-profiles).

### Deploy

To deploy or update the Serverless resources, from the project folder:

```sh
$ serverless deploy --stage {name} --project {name}
```

Stage name could be dev, staging, production, etc. Project name is a descriptor for the DynamoDB Table name and the CloudFormation stack name. **Both options are required**.

## Usage

Once the Serverless package has been deployed to your AWS account as a Lambda function and DynamoDB table, you can access the service call URLs for setting and getting a log entry. The alphanumeric ID will be generated by the deploy command which will output the Service Information upon completion. Region code defaults to us-east-1.

```
https://{some alphanumeric ID}.execute-api.{region code}.amazonaws.com/{stage name}/logs/(create | get)
```

### Create

* Method: POST
* Headers:
  * Accept \*/\*
  * Content-Type 'application/json'
* Body
  * JSON object (sent as a string) *all attributes are required*
    * user_id (string) - string ID to identify either the user or device
    * log_level (integer) - integer as the debug level for this log
    * device_info (string) - description of device information (type, OS, version, etc)
    * log_entry (string) - the full string of the log
    * created_at (string) - timestamp for when this log was created

### Get

* Method: GET
* Parameters: *Must contain at least 1 parameter in the service call*
  * LogLevel - filters log entries returned by the debug level
  * UserID - filters log entries by the user ID string field


## Support

Please [open an issue](https://bitbucket.org/copperhill/remote-logger/issues/new) for support.
